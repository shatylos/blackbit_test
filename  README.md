# README #

### Steps to run project: ###

* git clone https://shatylos@bitbucket.org/shatylos/blackbit_test.git
* docker-compose up -d
* docker exec -it php bash

Vendor directory already in git. Not perfect decision but I think it's ok for test task for quick deployment. 

* Apply migrations: bin/console doctrine:migration:migrate
* Load test data: bin/console doctrine:fixtures:load

Api endpoint should be available by link:
http://127.0.0.1:8080/api/v1/product/list

### Short description ###

Source data was a little bit changed. I mean column basePrice. I converted the values to price by KG. For storing in DB values of one type. And changed string value to float value. It allows using this field more flexible. I did it on the load fixture step. In real life, it better to do on import data step.

For now implemented and allowed to use 4 filters.
- price_min
- price_max
- region_name
- property_id

Filters by region and property expect string or array to apply multiple values by the filter.
region_name and property_id can be displayed in json response as applicable_filters with available values.

Filters by price expect float value.
price_min and price_max can not be displayed in applicable_filters. Just to keep the response structure clear. These filters can be applied at any time and they don't have a certain value to filter.

### Improvements and future support ###
To add a new filter just need to add new implementation to namespace App\Repository\Filter\Product and register the class in App\Repository\Filter\FilterFabric

Class ProductEntityRepository has methods findByFilters and findApplicableFilters. Both of the methods get filters that should be applied to display products. But logic split into different functions for future support. For example, findByFilters can work with pagination in the future. But findApplicableFilters always should get all products to correct filter values.

As improvement would be fine to implement caching of filter result data
