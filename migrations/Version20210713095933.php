<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210713095933 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE manufacturer_entity (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE product_entity (id INT AUTO_INCREMENT NOT NULL, region_id INT DEFAULT NULL, manufacturer_id INT DEFAULT NULL, manufacturer_code BIGINT NOT NULL, name VARCHAR(255) NOT NULL, price DOUBLE PRECISION NOT NULL, base_price_kg DOUBLE PRECISION DEFAULT NULL, INDEX IDX_6C5405CC98260155 (region_id), INDEX IDX_6C5405CCA23B42D (manufacturer_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE product_entity_product_property_entity (product_entity_id INT NOT NULL, product_property_entity_id INT NOT NULL, INDEX IDX_96C37B7EF85CBD0 (product_entity_id), INDEX IDX_96C37B721CFA9C (product_property_entity_id), PRIMARY KEY(product_entity_id, product_property_entity_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE product_property_entity (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE region_entity (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE product_entity ADD CONSTRAINT FK_6C5405CC98260155 FOREIGN KEY (region_id) REFERENCES region_entity (id)');
        $this->addSql('ALTER TABLE product_entity ADD CONSTRAINT FK_6C5405CCA23B42D FOREIGN KEY (manufacturer_id) REFERENCES manufacturer_entity (id)');
        $this->addSql('ALTER TABLE product_entity_product_property_entity ADD CONSTRAINT FK_96C37B7EF85CBD0 FOREIGN KEY (product_entity_id) REFERENCES product_entity (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE product_entity_product_property_entity ADD CONSTRAINT FK_96C37B721CFA9C FOREIGN KEY (product_property_entity_id) REFERENCES product_property_entity (id) ON DELETE CASCADE');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE product_entity DROP FOREIGN KEY FK_6C5405CCA23B42D');
        $this->addSql('ALTER TABLE product_entity_product_property_entity DROP FOREIGN KEY FK_96C37B7EF85CBD0');
        $this->addSql('ALTER TABLE product_entity_product_property_entity DROP FOREIGN KEY FK_96C37B721CFA9C');
        $this->addSql('ALTER TABLE product_entity DROP FOREIGN KEY FK_6C5405CC98260155');
        $this->addSql('DROP TABLE manufacturer_entity');
        $this->addSql('DROP TABLE product_entity');
        $this->addSql('DROP TABLE product_entity_product_property_entity');
        $this->addSql('DROP TABLE product_property_entity');
        $this->addSql('DROP TABLE region_entity');
    }
}
