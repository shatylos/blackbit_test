<?php

namespace App\Entity;

use App\Repository\ProductEntityRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=ProductEntityRepository::class)
 */
class ProductEntity
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="bigint")
     */
    private $manufacturerCode;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="float")
     */
    private $price;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $basePriceKg;

    /**
     * @ORM\ManyToOne(targetEntity=RegionEntity::class, inversedBy="products")
     */
    private $region;

    /**
     * @ORM\ManyToOne(targetEntity=ManufacturerEntity::class, inversedBy="products")
     */
    private $manufacturer;

    /**
     * @ORM\ManyToMany(targetEntity=ProductPropertyEntity::class, inversedBy="products")
     */
    private $properties;

    public function __construct()
    {
        $this->properties = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getManufacturerCode(): ?string
    {
        return $this->manufacturerCode;
    }

    public function setManufacturerCode(string $manufacturerCode): self
    {
        $this->manufacturerCode = $manufacturerCode;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getPrice(): ?float
    {
        return $this->price;
    }

    public function setPrice(float $price): self
    {
        $this->price = $price;

        return $this;
    }

    public function getBasePriceKg(): ?float
    {
        return $this->basePriceKg;
    }

    public function setBasePriceKg(?float $basePriceKg): self
    {
        $this->basePriceKg = $basePriceKg;

        return $this;
    }

    public function getRegion(): ?RegionEntity
    {
        return $this->region;
    }

    public function setRegion(?RegionEntity $region): self
    {
        $this->region = $region;

        return $this;
    }

    public function getManufacturer(): ?ManufacturerEntity
    {
        return $this->manufacturer;
    }

    public function setManufacturer(?ManufacturerEntity $manufacturer): self
    {
        $this->manufacturer = $manufacturer;

        return $this;
    }

    /**
     * @return Collection|ProductPropertyEntity[]
     */
    public function getProperties(): Collection
    {
        return $this->properties;
    }

    public function addProperty(ProductPropertyEntity $property): self
    {
        if (!$this->properties->contains($property)) {
            $this->properties[] = $property;
        }

        return $this;
    }

    public function removeProperty(ProductPropertyEntity $property): self
    {
        $this->properties->removeElement($property);

        return $this;
    }
}
