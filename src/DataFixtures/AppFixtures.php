<?php

namespace App\DataFixtures;

use App\Entity\ManufacturerEntity;
use App\Entity\ProductEntity;
use App\Entity\ProductPropertyEntity;
use App\Entity\RegionEntity;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class AppFixtures extends Fixture
{

    /**
     * Position of column in the CSV file
     */
    const MANUFACTURER_CODE = 0;
    const NAME = 1;
    const REGION_NAME = 2;
    const PRICE = 3;
    const BASE_PRICE = 4;
    const MANUFACTURER_NAME = 5;
    const PROPERTIES = 6;

    /**
     * Store the values for avoiding duplicates in DB
     */
    private $regions = [];
    private $manufacturers = [];
    private $properties = [];

    public function load(ObjectManager $manager)
    {
        if (($fp = fopen(__DIR__ . DIRECTORY_SEPARATOR . "data" . DIRECTORY_SEPARATOR . "pasta.csv", "r")) !== FALSE) {

            $rowNo = 0;

            while (($row = fgetcsv($fp, 1000, ";")) !== FALSE) {

                if (count($row) == 7) {

                    $region = null;
                    if ($row[$this::REGION_NAME]) {
                        $region = $this->getOrCreateRegion($row[$this::REGION_NAME]);
                        if (!$region->getId()) {
                            $manager->persist($region);
                        }
                    }

                    $manufacturer = null;
                    if ($row[$this::MANUFACTURER_NAME]) {
                        $manufacturer = $this->getOrCreateManufacturer($row[$this::MANUFACTURER_NAME]);
                        if (!$manufacturer->getId()) {
                            $manager->persist($manufacturer);
                        }
                    }

                    $properties = [];
                    if ($row[$this::PROPERTIES]) {
                        $properties = $this->getOrCreateProperties($row[$this::PROPERTIES]);
                        foreach ($properties as $property) {
                            if (!$property->getId()) {
                                $manager->persist($property);
                            }
                        }
                    }

                    $product = $this->createProduct($row, $region, $manufacturer, $properties);
                    $manager->persist($product);

                    $rowNo++;
                }
            }

            $manager->flush();
            fclose($fp);

            echo "Was added {$rowNo} products";
        }
    }

    /**
     * @param array $row Row from CSV file
     * @param RegionEntity|null $region
     * @param ManufacturerEntity|null $manufacturer
     * @param ProductPropertyEntity[] $properties
     * @return ProductEntity
     */
    private function createProduct(array $row,
                                   RegionEntity $region = null,
                                   ManufacturerEntity $manufacturer = null,
                                   array $properties = []
    ): ProductEntity
    {
        $product = new ProductEntity();
        $product->setManufacturerCode($row[$this::MANUFACTURER_CODE]);
        $product->setName($row[$this::NAME]);
        $product->setPrice($row[$this::PRICE]);
        $product->setBasePriceKg($this->getBasePriceByKg($row[$this::BASE_PRICE]));

        if ($region) {
            $product->setRegion($region);
        }

        if ($manufacturer) {
            $product->setManufacturer($manufacturer);
        }

        foreach ($properties as $property) {
            $product->addProperty($property);
        }

        return $product;
    }

    /**
     * Convert base price from string to float. Convert cost value always by KG
     *
     * @param $priceString
     * @return float
     */
    private function getBasePriceByKg($priceString): float
    {
        $priceByKg = 0.0;

        if (strpos($priceString, " €/KG") !== false) {
            $priceString = str_replace(" €/KG", "", $priceString);
            $priceString = str_replace(",", ".", $priceString);
            $priceByKg = (float) $priceString;
        } elseif (strpos($priceString, " €/100 g") !== false) {
            $priceString = str_replace(" €/100 g", "", $priceString);
            $priceString = str_replace(",", ".", $priceString);
            $priceByKg = (float) $priceString * 10;
        }

        return $priceByKg;
    }

    /**
     * @param string $regionName
     * @return RegionEntity
     */
    private function getOrCreateRegion(string $regionName): RegionEntity
    {
        if (empty($this->regions[$regionName])) {
            $region = new RegionEntity();
            $region->setName($regionName);
            $this->regions[$regionName] = $region;
        }

        return $this->regions[$regionName];
    }

    /**
     * @param string $manufacturerName
     * @return ManufacturerEntity
     */
    private function getOrCreateManufacturer(string $manufacturerName): ManufacturerEntity
    {
        if (empty($this->manufacturers[$manufacturerName])) {
            $manufacturer = new ManufacturerEntity();
            $manufacturer->setName($manufacturerName);
            $this->manufacturers[$manufacturerName] = $manufacturer;
        }

        return $this->manufacturers[$manufacturerName];
    }

    /**
     * @param string $propertiesString
     * @return ProductPropertyEntity[]
     */
    private function getOrCreateProperties(string $propertiesString): array
    {
        $properties = [];
        $propertiesName = explode(",", $propertiesString);

        foreach ($propertiesName as $propertyName) {
            if (empty($this->properties[$propertyName])) {
                $property = new ProductPropertyEntity();
                $property->setName($propertyName);
                $this->properties[$propertyName] = $property;
            }
            $properties[$propertyName] = $this->properties[$propertyName];
        }

        return $properties;
    }

}
