<?php

namespace App\Repository;

use App\Entity\ProductEntity;
use App\Repository\Filter\FilterFabric;
use App\Repository\Filter\FilterInterface;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method ProductEntity|null find($id, $lockMode = null, $lockVersion = null)
 * @method ProductEntity|null findOneBy(array $criteria, array $orderBy = null)
 * @method ProductEntity[]    findAll()
 * @method ProductEntity[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ProductEntityRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ProductEntity::class);
    }

    /**
     * @param FilterInterface $filter
     * @return array
     */
    public function findByFilters(FilterInterface $filter)
    {
        $queryBuilder = $this->createQueryBuilder('p');
        $filter->apply($queryBuilder);
        return $queryBuilder->getQuery()->getResult();
    }

    /**
     * @param FilterInterface $filter
     * @return array
     */
    public function findApplicableFilters(FilterInterface $filter): array
    {
        $applicableFilters = [];
        $queryBuilder = $this->createQueryBuilder('p');
        $filter->apply($queryBuilder);

        $products = $queryBuilder->getQuery()->getResult();
        $filterList = FilterFabric::getNotAppliedFilterList();

        foreach ($filterList as $filterKey => $filterClassName) {
            $applicableFilters[$filterKey] = [];
            foreach ($products as $product) {
                if ($allowedValues = $filterClassName::getAllowedValues($product)) {
                    $applicableFilters[$filterKey] = array_merge($applicableFilters[$filterKey], $allowedValues);
                }
            }

            $applicableFilters[$filterKey] = $this->convertToUniqueCounted($applicableFilters[$filterKey]);

            if (empty($applicableFilters[$filterKey])) {
                unset($applicableFilters[$filterKey]);
            }
        }

        return $applicableFilters;
    }

    /**
     * @param $filters
     * @return array
     */
    private function convertToUniqueCounted($filters)
    {
        $filterResult = [];

        foreach ($filters as $filterItem) {
            if (empty($filterItem["filter_by"])) {
                continue;
            }
            if (empty($filterResult[$filterItem["filter_by"]])) {
                $filterResult[$filterItem["filter_by"]] = $filterItem;
                $filterResult[$filterItem["filter_by"]]["count"] = 1;
            } else {
                $filterResult[$filterItem["filter_by"]]["count"]++;
            }
        }

        return array_values($filterResult);
    }

}
