<?php

namespace App\Repository\Filter;


use App\Repository\Filter\Product\PriceMaxFilter;
use App\Repository\Filter\Product\PriceMinFilter;
use App\Repository\Filter\Product\PropertyFilter;
use App\Repository\Filter\Product\RegionNameFilter;
use Symfony\Component\HttpFoundation\Request;

class FilterFabric
{

    /**
     * @var string[] Mapping of request parameters to filter implementation class
     */
    private static $paramToClassMapping = [
        "price_min" => PriceMinFilter::class,
        "price_max" => PriceMaxFilter::class,
        "region_name" => RegionNameFilter::class,
        "property_id" => PropertyFilter::class,
    ];

    /**
     * Get decorated filter by all requested parameters
     * @return FilterInterface
     */
    public static function getProductFilterByRequest(): FilterInterface
    {
        $request = Request::createFromGlobals();
        $filter = new BaseFilter();
        foreach (self::$paramToClassMapping as $param => $className) {
            if ($request->get($param)) {
                $filter = new $className($filter, $request->get($param));
            }
        }

        return $filter;
    }

    /**
     * Get list fo filters not applied in request
     * @return array
     */
    public static function getNotAppliedFilterList(): array
    {
        $notAppliedFilters = [];
        $request = Request::createFromGlobals();
        foreach (self::$paramToClassMapping as $param => $className) {
            if (!$request->get($param)) {
                $notAppliedFilters[$param] = $className;
            }
        }
        return $notAppliedFilters;
    }

}
