<?php

namespace App\Repository\Filter;

use App\Entity\ProductEntity;
use Doctrine\ORM\QueryBuilder;

abstract class AbstractFilterDecorator implements FilterInterface
{

    /**
     * @var FilterInterface
     */
    protected $parentFilter;

    /**
     * @var string|int|float|array Value for filter by
     */
    protected $context;

    /**
     * AbstractFilterDecorator constructor.
     * @param FilterInterface $parentFilter
     * @param $context
     */
    public function __construct(FilterInterface $parentFilter, $context)
    {
        $this->parentFilter = $parentFilter;
        $this->context = $context;
    }

    /**
     * Apply the filter condition to queryBuilder
     * @param QueryBuilder $queryBuilder
     */
    public function apply(QueryBuilder $queryBuilder)
    {
        $this->parentFilter->apply($queryBuilder);
    }

    /**
     * List of allowed values for the filter.
     * Key filter_by is mandatory in result for correct filtering by unique values
     * @param ProductEntity $product
     * @return array
     */
    public static function getAllowedValues(ProductEntity $product): array
    {
        return [];
    }

}