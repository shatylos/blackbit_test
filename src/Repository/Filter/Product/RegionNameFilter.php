<?php

namespace App\Repository\Filter\Product;

use App\Entity\ProductEntity;
use App\Entity\RegionEntity;
use App\Repository\Filter\AbstractFilterDecorator;
use Doctrine\ORM\Query\Expr\Join;
use Doctrine\ORM\QueryBuilder;

class RegionNameFilter extends AbstractFilterDecorator
{

    /**
     * @param QueryBuilder $queryBuilder
     */
    public function apply(QueryBuilder $queryBuilder)
    {
        $queryBuilder->leftJoin(RegionEntity::class, "r", Join::WITH, "p.region = r");

        if (is_array($this->context)) {
            $values = array_values($this->context);
            if (count($values) > 0) {
                $orExpr = $queryBuilder->expr()->orX();
                for ($i = 0, $iMax = count($values); $i < $iMax; $i++) {
                    $orExpr->add($queryBuilder->expr()->eq('r.name', ":region_name_{$i}"));
                    $queryBuilder->setParameter("region_name_{$i}", $values[$i]);
                }
                $queryBuilder->andWhere($orExpr);
            }
        } else {
            $queryBuilder->andWhere('r.name = :region_name');
            $queryBuilder->setParameter('region_name', $this->context);
        }

        parent::apply($queryBuilder);
    }

    /**
     * @param ProductEntity $product
     * @return array|array[]
     */
    public static function getAllowedValues(ProductEntity $product): array
    {
        return ($product->getRegion()) ? [[
            'id' => $product->getRegion()->getId(),
            'name' => $product->getRegion()->getName(),
            'filter_by' => $product->getRegion()->getName(),
        ]] : [];
    }

}
