<?php

namespace App\Repository\Filter\Product;

use App\Repository\Filter\AbstractFilterDecorator;
use Doctrine\ORM\QueryBuilder;

class PriceMinFilter extends AbstractFilterDecorator
{

    /**
     * @param QueryBuilder $queryBuilder
     */
    public function apply(QueryBuilder $queryBuilder)
    {
        $queryBuilder->andWhere('p.price >= :price_min');
        $queryBuilder->setParameter('price_min', $this->context);
        parent::apply($queryBuilder);
    }

}
