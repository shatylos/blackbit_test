<?php

namespace App\Repository\Filter\Product;

use App\Repository\Filter\AbstractFilterDecorator;
use Doctrine\ORM\QueryBuilder;

class PriceMaxFilter extends AbstractFilterDecorator
{

    /**
     * @param QueryBuilder $queryBuilder
     */
    public function apply(QueryBuilder $queryBuilder)
    {
        $queryBuilder->andWhere('p.price <= :price_max');
        $queryBuilder->setParameter('price_max', $this->context);
        parent::apply($queryBuilder);
    }

}
