<?php

namespace App\Repository\Filter\Product;

use App\Entity\ProductEntity;
use App\Repository\Filter\AbstractFilterDecorator;
use Doctrine\ORM\QueryBuilder;

class PropertyFilter extends AbstractFilterDecorator
{

    /**
     * @param QueryBuilder $queryBuilder
     */
    public function apply(QueryBuilder $queryBuilder)
    {
        $queryBuilder->andWhere(':propertyId MEMBER OF p.properties');
        $queryBuilder->setParameter('propertyId', $this->context);

        parent::apply($queryBuilder);
    }

    /**
     * @param ProductEntity $product
     * @return array
     */
    public static function getAllowedValues(ProductEntity $product): array
    {
        $values = [];
        foreach ($product->getProperties() as $property) {
            $values[] = [
                'id' => $property->getId(),
                'name' => $property->getName(),
                'filter_by' => $property->getId(),
            ];
        }
        return $values;
    }

}
