<?php

namespace App\Controller\Api;

use App\Repository\Filter\FilterFabric;
use App\Repository\ProductEntityRepository;
use App\ResponseNormilizer\ProductNormilizer;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/api/v1", name="api_1_", format="json")
 */
class ProductListController extends AbstractApiController
{

    private $productEntityRepository;

    private $productNormalizer;

    /**
     * ProductListController constructor.
     * @param ProductEntityRepository $productEntityRepository
     * @param ProductNormilizer $productNormalizer
     */
    public function __construct(ProductEntityRepository $productEntityRepository, ProductNormilizer $productNormalizer)
    {
        $this->productEntityRepository = $productEntityRepository;
        $this->productNormalizer = $productNormalizer;
    }

    /**
     * @Route("/product/list", name="product_list", methods={"GET"})
     */
    public function index(): Response
    {
        $data = [];
        $meta = [];

        $filter = FilterFabric::getProductFilterByRequest();
        $products = $this->productEntityRepository->findByFilters($filter);

        $meta['applicable_filters'] = $this->productEntityRepository->findApplicableFilters($filter);

        foreach ($products as $product) {
            $data[] = $this->productNormalizer->normalize($product);
        }

        return $this->jsonResponse($data, $meta);
    }
}
