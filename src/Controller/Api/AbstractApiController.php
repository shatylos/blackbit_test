<?php

namespace App\Controller\Api;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;

abstract class AbstractApiController extends AbstractController
{

    /**
     * @param $data
     * @param array $meta
     * @return JsonResponse
     */
    protected function jsonResponse($data, $meta = []): JsonResponse
    {
        $responseData['data'] = $data;
        $responseData['meta'] = $meta;

        return $this->json($responseData);
    }

}
