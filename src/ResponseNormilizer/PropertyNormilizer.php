<?php

namespace App\ResponseNormilizer;

use App\Entity\ProductPropertyEntity;

class PropertyNormilizer
{

    /**
     * @param ProductPropertyEntity $productPropertyEntity
     * @return array
     */
    public function normalize(ProductPropertyEntity $productPropertyEntity)
    {
        $responseProperty = [];
        $responseProperty["id"] = $productPropertyEntity->getId();
        $responseProperty["name"] = $productPropertyEntity->getName();
        return $responseProperty;
    }
}