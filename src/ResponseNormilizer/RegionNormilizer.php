<?php

namespace App\ResponseNormilizer;

use App\Entity\RegionEntity;

class RegionNormilizer
{

    /**
     * @param RegionEntity $regionEntity
     * @return array
     */
    public function normalize(RegionEntity $regionEntity)
    {
        $responseRegion = [];
        $responseRegion["id"] = $regionEntity->getId();
        $responseRegion["name"] = $regionEntity->getName();
        return $responseRegion;
    }
}