<?php

namespace App\ResponseNormilizer;

use App\Entity\ManufacturerEntity;

class ManufacturerNormilizer
{

    /**
     * @param ManufacturerEntity $manufacturerEntity
     * @return array
     */
    public function normalize(ManufacturerEntity $manufacturerEntity)
    {
        $responseManufacturer = [];
        $responseManufacturer["id"] = $manufacturerEntity->getId();
        $responseManufacturer["name"] = $manufacturerEntity->getName();
        return $responseManufacturer;
    }
}