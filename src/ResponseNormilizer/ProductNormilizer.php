<?php

namespace App\ResponseNormilizer;

use App\Entity\ProductEntity;

class ProductNormilizer
{

    /**
     * @var RegionNormilizer
     */
    private $regionNormilizer;

    /**
     * @var ManufacturerNormilizer
     */
    private $manufacturerNormilizer;

    /**
     * @var PropertyNormilizer
     */
    private $propertyNormilizer;

    /**
     * ProductNormilizer constructor.
     * @param $regionNormilizer
     * @param $manufacturerNormilizer
     * @param $propertyNormilizer
     */
    public function __construct(RegionNormilizer $regionNormilizer,
                                ManufacturerNormilizer $manufacturerNormilizer,
                                PropertyNormilizer $propertyNormilizer)
    {
        $this->regionNormilizer = $regionNormilizer;
        $this->manufacturerNormilizer = $manufacturerNormilizer;
        $this->propertyNormilizer = $propertyNormilizer;
    }

    /**
     * @param ProductEntity $productEntity
     * @return array
     */
    public function normalize(ProductEntity $productEntity)
    {
        $responseProduct = [];
        $responseProduct['id'] = $productEntity->getId();
        $responseProduct['manufacturer_code'] = $productEntity->getManufacturerCode();
        $responseProduct['name'] = $productEntity->getName();
        $responseProduct['price'] = $productEntity->getPrice();
        $responseProduct['base_price_kg'] = $productEntity->getBasePriceKg();
        if ($productEntity->getRegion()) {
            $responseProduct['region'] = $this->regionNormilizer->normalize($productEntity->getRegion());
        }
        if ($productEntity->getManufacturer()) {
            $responseProduct['manufacturer'] = $this->manufacturerNormilizer->normalize($productEntity->getManufacturer());
        }
        if ($productEntity->getProperties()) {
            $responseProduct['properties'] = [];
            foreach ($productEntity->getProperties() as $property) {
                $responseProduct['properties'][] = $this->propertyNormilizer->normalize($property);
            }
        }

        return $responseProduct;
    }

}
